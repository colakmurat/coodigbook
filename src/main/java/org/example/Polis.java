package org.example;

public class Polis extends Memur implements Ikisi{
    String birim;
    double maas;

    public Polis(String ad, String id, int sicil, double taban, String birim) {
        super(ad, id, sicil,taban);
        this.birim = birim;
    }

    @Override
    public double maasHesapla()
    {
        maas=taban*0.9;
        return maas;
    }

    @Override
    public String  denemeX() {
        return "polis denemesi";
    }

    @Override
    public String  biriminiSoyle() {
        return "polisin calıştığı birim"+birim;
    }

    @Override
    public String toString() {
        return "Polis{" +
                "birim='" + birim + '\'' +
                ", ad='" + ad + '\'' +
                ", id='" + id + '\'' +
                ", sicil=" + sicil +
                ", maaş=" + maasHesapla() +
                '}';
    }

    public String yazdir(){
        return "Polis memuru maasi: "+maasHesapla()+"\n"+ biriminiSoyle()+"\n"+denemeX()+"\n";
    }

}
