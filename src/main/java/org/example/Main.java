package org.example;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        ArrayList<Ikisi> kisler = new ArrayList<>();
        double taban=1000;
        Ikisi polis = new Polis("mustafa","93128",93128,taban,"Trafik");
        Ikisi ogretmen = new Ogretmen("Hasan","7896",1236,taban,"Tarih");
        Ikisi isci = new Isci("Hasan",taban);
        Ikisi polis2=new Polis("murat","123023",7410,taban,"Terör");


        kisler.add(ogretmen);
        kisler.add(polis);
        kisler.add(isci);
        kisler.add(polis2);

        String contex = "";
        FileWriter fileWriter = new FileWriter("src\\main\\resources\\kisiler.txt", false);
        String content = new String(Files.readAllBytes(Paths.get("C:\\Users\\murat\\IdeaProjects\\Coodingbook\\src\\main\\resources\\kisiler.txt")));


        for (Ikisi k : kisler) {
            System.out.println(k.toString());
            content = k.toString()+"\n";
            k.yazdir();

            fileWriter.write(content);
            System.out.println("---------");
        }
        fileWriter.close();


    }
}