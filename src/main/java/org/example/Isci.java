package org.example;

public class Isci implements Ikisi,Deneme{
    String IsciName;
    double maas;
    double taban;

    public Isci(String isciName, double taban) {
        IsciName = isciName;
        this.taban=taban;
    }

    @Override
    public double maasHesapla()
    {
        maas=taban*0.4;
        return maas;
    }

    @Override
    public String biriminiSoyle() {
        return" inşaat işçisi";
    }

    @Override
    public String  denemeX() {
      return "işçi denemesi";
    }

    @Override
    public String toString() {
        return "Isci{" +
                "IsciName='" + IsciName + '\'' +
                '}';
    }

    public String yazdir(){
        return toString()+"\n"+"İşci maaşı: "+maasHesapla()+"\n"+ biriminiSoyle()+"\n"+denemeX()+"\n";
    }
}
