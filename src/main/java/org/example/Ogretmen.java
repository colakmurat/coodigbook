package org.example;

public class Ogretmen extends Memur implements Ikisi{
    private String ders;double maas;

    public Ogretmen(String ad, String id, int sicil, double taban, String ders) {
        super(ad, id, sicil,taban);
        this.ders = ders;
    }

    @Override
    public double maasHesapla()
    {
        maas=taban*0.9;
        return maas;
    }

    @Override
    public String denemeX() {
        return "oğretmen denemesi";
    }
    @Override
    public String biriminiSoyle() {
        return "oğretmen birimi tarih";
    }

    @Override
    public String toString() {
        return "Ogretmen{" +
                "ad='" + ad + '\'' +
                ", id='" + id + '\'' +
                ", sicil=" + sicil +
                ", maaş=" + maasHesapla()+
                '}';
    }

    @Override

    public String yazdir(){
        return toString()+"\n"+"Oğretmenin maaşı: "+maas+"\n"+ biriminiSoyle()+"\n"+denemeX()+"\n";
    }
}
