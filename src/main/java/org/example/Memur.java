package org.example;

public abstract class Memur implements Ikisi, Deneme {

    String ad;
    String id;
    int sicil;
    double maas;
    double taban;

    public Memur(String ad, String id, int sicil,double taban ) {
        this.ad = ad;
        this.id = id;
        this.sicil = sicil;
        this.taban=taban;
    }


    @Override
    public double maasHesapla()
    {
        maas=taban*0.9;
        return maas;
    }

    @Override
    public String biriminiSoyle() {
        return "0";
    }

    @Override
    public String denemeX() {
        return "0";
    }

    @Override
    public String toString() {
        return "Memur{" +
                "ad='" + ad + '\'' +
                ", id='" + id + '\'' +
                ", sicil=" + sicil +
                "maaş='" + maasHesapla()+ '\'' +
                '}';
    }
}
